import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Listing from '../views/Listing'
import New from "../views/New"
import ListeShow from "../views/ListeShow"
import NewItem from "../views/NewItem"

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/listing',
    name: 'Listing',
    component: Listing
  },
  {
    path: '/new',
    name: 'New',
    component: New
  },
  {
    path: '/list/:id',
    name: 'ListShow',
    component: ListeShow,
    props: true
  },
  {
    path: '/list/:id/add',
    name: 'NewItem',
    component: NewItem,
    props: true
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
